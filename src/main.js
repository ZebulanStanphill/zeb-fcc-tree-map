// External dependencies
import { hierarchy, treemap } from 'd3-hierarchy';
import { select } from 'd3-selection';

// Project asset dependencies
import data from './assets/data.json';

const height = 500;
const width = 1000;

let mouseX, mouseY;

document.addEventListener(
	'mousemove',
	( { pageX, pageY } ) => {
		mouseX = pageX;
		mouseY = pageY;
	}
);

const root = select( '#app-root' );

// Append title to root element.
root.append( 'h1' )
	.attr( 'id', 'title' )
	.text( 'Best-Selling Video Games' );

// Append description to root element.
root.append( 'p' )
	.attr( 'id', 'description' )
	.text( 'Tree map of the top 100 best-selling video games.' );

// Append svg to root element.
const svg = root.append( 'svg' )
	.attr( 'class', 'tree-map' )
	.attr( 'viewBox', `0 0 ${ width } ${ height }` );

// Create hierarchy root node from data JSON.
// https://github.com/d3/d3-hierarchy/blob/master/README.md#hierarchy
const dataHierarchyRoot = hierarchy( data )
	.sum( d => d.value )
	.sort(
		( a, b ) => {
			return b.value - a.value;
		}
	);

const dataTreeMap = treemap()
	.size( [ width, height ] )
	.padding( 1 )( dataHierarchyRoot );

const consoleColors = {
	'2600': '#840',
	'3DS': '#f00',
	'DS': '#888',
	'GB': '#474',
	'GBA': '#70f',
	'N64': '#f90',
	'NES': '#900',
	'PC': '#ff0',
	'PS': '#ddd',
	'PS2': '#4af',
	'PS3': '#444',
	'PS4': '#22f',
	'PSP': '#d0d',
	'SNES': '#88f',
	'Wii': '#fff',
	'X360': '#6f6',
	'XB': '#0d0',
	'XOne': '#080'
};

svg.selectAll( '.tile' ) // Select the tiles.
	.data( dataTreeMap.leaves() )
	.enter()
	.append( 'rect' ) // Create the game tiles.
	.attr( 'class', 'tile' )
	.attr( 'transform', d => `translate(${d.x0}, ${d.y0 + 40})` )
	.attr( 'width', d => d.x1 - d.x0 )
	.attr( 'height', d => d.y1 - d.y0 )
	.attr( 'data-name', d => d.data.name )
	.attr( 'data-category', d => d.data.category )
	.attr( 'data-value', d => d.value )
	.attr(
		'fill',
		d => {
			switch ( d.data.category ) {
				case '2600':
					return consoleColors['2600'];
				case '3DS':
					return consoleColors['3DS'];
				case 'DS':
					return consoleColors['DS'];
				case 'GB':
					return consoleColors['GB'];
				case 'GBA':
					return consoleColors['GBA'];
				case 'N64':
					return consoleColors['N64'];
				case 'NES':
					return consoleColors['NES'];
				case 'PC':
					return consoleColors['PC'];
				case 'PS':
					return consoleColors['PS'];
				case 'PS2':
					return consoleColors['PS2'];
				case 'PS3':
					return consoleColors['PS3'];
				case 'PS4':
					return consoleColors['PS4'];
				case 'PSP':
					return consoleColors['PSP'];
				case 'SNES':
					return consoleColors['SNES'];
				case 'Wii':
					return consoleColors['Wii'];
				case 'X360':
					return consoleColors['X360'];
				case 'XB':
					return consoleColors['XB'];
				case 'XOne':
					return consoleColors['XOne'];
				default:
					return '#000';
			}
		}
	)
	.on( 'mousemove', function() {
		const name = this.getAttribute( 'data-name' );
		const category = this.getAttribute( 'data-category' );
		const value = this.getAttribute( 'data-value' );

		select( '#tooltip' )
			.attr( 'data-value', value )
			.style( 'display', 'block' )
			.style( 'position', 'absolute' )
			.style( 'left', `${mouseX + 16}px` )
			.style( 'top', `${mouseY - 80}px` )
			.html(
				`Name: ${name}<br />` +
				`Platform: ${category}`
			);
	} )
	.on( 'mouseleave', function() {
		select( '#tooltip' )
			.style( 'display', 'none' );
	} );

const legend = svg.append( 'g' )
	.attr( 'id', 'legend' )
	.attr( 'transform', 'translate(0, 10)' );

// Create dataset for programmatically generating legend items.
const legendData = [
	{
		'name': 'Atari 2600',
		'color': consoleColors['2600']
	},
	{
		'name': 'NES',
		'color': consoleColors['NES'],
		'x': 80
	},
	{
		'name': 'SNES',
		'color': consoleColors['SNES'],
		'x': 125
	},
	{
		'name': 'N64',
		'color': consoleColors['N64'],
		'x': 180
	},
	{
		'name': 'Wii',
		'color': consoleColors['Wii'],
		'x': 225
	},
	{
		'name': 'GB',
		'color': consoleColors['GB'],
		'x': 265
	},
	{
		'name': 'GBA',
		'color': consoleColors['GBA'],
		'x': 305,
		'y': 0
	},
	{
		'name': 'DS',
		'color': consoleColors['DS'],
		'x': 355,
		'y': 0
	},
	{
		'name': '3DS',
		'color': consoleColors['3DS'],
		'x': 395,
		'y': 0
	},
	{
		'name': 'PS1',
		'color': consoleColors['PS'],
		'x': 440,
		'y': 0
	},
	{
		'name': 'PS2',
		'color': consoleColors['PS2'],
		'x': 485,
		'y': 0
	},
	{
		'name': 'PS3',
		'color': consoleColors['PS3'],
		'x': 530,
		'y': 0
	},
	{
		'name': 'PS4',
		'color': consoleColors['PS4'],
		'x': 575,
		'y': 0
	},
	{
		'name': 'PSP',
		'color': consoleColors['PSP'],
		'x': 620,
		'y': 0
	},
	{
		'name': 'Xbox',
		'color': consoleColors['XB'],
		'x': 665,
		'y': 0
	},
	{
		'name': 'Xbox 360',
		'color': consoleColors['X360'],
		'x': 715,
		'y': 0
	},
	{
		'name': 'Xbox One',
		'color': consoleColors['XOne'],
		'x': 790,
		'y': 0
	},
	{
		'name': 'PC (Windows)',
		'color': consoleColors['PC'],
		'x': 870
	},
];

legend.selectAll( '.legend-item-container' )
	.data( legendData )
	.enter()
	.append( 'g' )
	.attr( 'class', 'legend-item-container' )
	.attr( 'transform', d => `translate(${d.x || 0 }, ${d.y || 0 })` )
	.append( 'rect' )
	.attr( 'class', 'legend-item' )
	.attr( 'width', 12 )
	.attr( 'height', 12 )
	.attr( 'fill', d => d.color )
	.select( function() { return this.parentNode; } )
	.append( 'text' )
	.attr( 'class', 'legend-text' )
	.attr( 'x', 14 )
	.attr( 'y', 8 )
	.attr( 'font-weight', '600' )
	.text( d => d.name );

root.append( 'p' )
	.attr( 'id', 'tooltip' )
	.style( 'display', 'none' );